package com.dnod.whoisthere.utils;

import android.content.Context;
import android.content.SharedPreferences;

public final class PreferencesHelper {
    private static final String PREFERENCES_NAME = "local_preferences";

    private static final String KEY_USER_NAME = "user_name";
    private static final String KEY_USER_PASS = "user_pass";

    private static PreferencesHelper sInstance;
    private final SharedPreferences mSharedPreferences;

    private PreferencesHelper(Context context) {
        mSharedPreferences = openPreferences(context);
    }

    public static void init(Context context) {
        sInstance = new PreferencesHelper(context);
    }

    public static void saveUserCredentials(String username, String password) {
        sInstance.openEditor().putString(KEY_USER_NAME, username)
                .putString(KEY_USER_PASS, password).apply();
    }

    public static String getUserName() {
        return sInstance.mSharedPreferences.getString(KEY_USER_NAME, null);
    }

    public static String getUserPassword() {
        return sInstance.mSharedPreferences.getString(KEY_USER_PASS, null);
    }

    public static void clearData() {
        sInstance.openEditor().clear().apply();
    }

    private SharedPreferences openPreferences(Context context) {
        return context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
    }

    private SharedPreferences.Editor openEditor() {
        return mSharedPreferences.edit();
    }
}
