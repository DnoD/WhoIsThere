package com.dnod.whoisthere.service

import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import android.support.v4.util.Pair
import com.dnod.whoisthere.R
import com.dnod.whoisthere.data.ChatMessage
import com.dnod.whoisthere.data.Status
import com.dnod.whoisthere.utils.PreferencesHelper
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import org.jivesoftware.smack.AbstractXMPPConnection
import org.jivesoftware.smack.ConnectionListener
import org.jivesoftware.smack.XMPPConnection
import org.jivesoftware.smack.tcp.XMPPTCPConnection
import java.lang.Exception
import io.reactivex.subjects.ReplaySubject
import org.jivesoftware.smack.chat2.Chat
import org.jivesoftware.smack.chat2.ChatManager
import org.jxmpp.jid.EntityBareJid
import org.jivesoftware.smack.chat2.IncomingChatMessageListener
import org.jivesoftware.smack.chat2.OutgoingChatMessageListener
import org.jivesoftware.smack.packet.Message
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration
import org.jxmpp.jid.impl.JidCreate

class ChatService : Service() {

    private val chatSubject = ReplaySubject.create<ChatMessage>()
    private val connectionSubject = PublishSubject.create<Status>()
    private val loginSubject = PublishSubject.create<Boolean>()
    private val mBinder = ChatBinder()
    private var chatConnection: AbstractXMPPConnection? = null
    private var chat: Chat? = null
    private var chatManager: ChatManager? = null
    private val connectionListener: ConnectionListener = object : ConnectionListener {
        override fun connected(connection: XMPPConnection?) {
            connectionSubject.onNext(Status.CONNECTED)
            initChat()
        }

        override fun connectionClosed() {
            connectionSubject.onNext(Status.DISCONNECTED)
        }

        override fun connectionClosedOnError(e: Exception?) {
            connectionSubject.onNext(Status.DISCONNECTED)
        }

        override fun reconnectionSuccessful() {
            connectionSubject.onNext(Status.CONNECTED)
        }

        override fun authenticated(connection: XMPPConnection?, resumed: Boolean) {
            connectionSubject.onNext(Status.CONNECTED)
        }

        override fun reconnectionFailed(e: Exception?) {
            connectionSubject.onNext(Status.DISCONNECTED)
        }

        override fun reconnectingIn(seconds: Int) {
        }

    }

    override fun onCreate() {
        super.onCreate()
    }

    override fun onBind(intent: Intent): IBinder? {
        return mBinder
    }

    override fun onRebind(intent: Intent) {
        super.onRebind(intent)
    }

    override fun onUnbind(intent: Intent): Boolean {
        return true
    }

    override fun onDestroy() {
        super.onDestroy()
        chatConnection!!.disconnect()
        chatConnection!!.removeConnectionListener(connectionListener)
        chatSubject.onComplete()
        connectionSubject.onComplete()
    }

    fun login(username: String, password: String) : Observable<Boolean> {
        val credentials = Pair(username, password)
        Observable.just(credentials)
                .subscribeOn(Schedulers.io())
                .subscribe({ data ->
                    if (chatConnection == null) {
                        val serviceName = JidCreate.domainBareFrom(getString(R.string.chat_server_hostname));
                        val config = XMPPTCPConnectionConfiguration.builder()
                                .setServiceName(serviceName)
                                .setUsernameAndPassword(data.first, data.second)
                                .setKeystoreType(null)
                                .build();
                        chatConnection = XMPPTCPConnection(config)
                        chatConnection!!.addConnectionListener(connectionListener)
                    }
                    if (!chatConnection!!.isConnected) {
                        chatConnection!!.connect().login()
                    }
                    val isConnected = chatConnection!!.isConnected
                    if (isConnected) {
                        PreferencesHelper.saveUserCredentials(data.first, data.second)
                    }
                    loginSubject.onNext(isConnected)
                }, { throwable ->
                    loginSubject.onNext(false)
                })
        return loginSubject.hide()
    }

    fun logout() {
        PreferencesHelper.clearData()
    }

    fun connect(userId: String) {
        val jid = JidCreate.entityBareFrom(userId + "@" + getString(R.string.chat_server_hostname))
        chat = chatManager!!.chatWith(jid)
    }

    fun send(message: String) {
        chat!!.send(message)
    }

    fun subscribeConnectionState() : Observable<Status> {
        return connectionSubject.hide()
    }

    fun subscribeChat(): Observable<ChatMessage> {
        return chatSubject.hide()
    }

    private fun initChat() {
        chatManager = ChatManager.getInstanceFor(chatConnection)
        chatManager!!.addIncomingListener(object : IncomingChatMessageListener {
            override fun newIncomingMessage(from: EntityBareJid?, message: Message?, chat: Chat?) {
                val chatMessage = ChatMessage()
                chatMessage.body = message!!.body
                chatMessage.ownerId = from!!.asEntityBareJidString().split("@")[0]
                chatSubject.onNext(chatMessage)
            }

        })
        chatManager!!.addOutgoingListener(object : OutgoingChatMessageListener {
            override fun newOutgoingMessage(to: EntityBareJid?, message: Message?, chat: Chat?) {
                val chatMessage = ChatMessage()
                chatMessage.body = message!!.body
                chatMessage.ownerId = PreferencesHelper.getUserName()
                chatSubject.onNext(chatMessage)
            }

        })
    }

    inner class ChatBinder : Binder() {
        internal val service: ChatService
            get() = this@ChatService
    }
}
