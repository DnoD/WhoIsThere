package com.dnod.whoisthere.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class ConnectivityReceiver extends BroadcastReceiver {

    private static AtomicBoolean isConnected = new AtomicBoolean(false);
    private static AtomicInteger connectionType = new AtomicInteger();

    public enum ConnectionType {
        MOBILE, WIFI
    }

    @Override
    public void onReceive(Context context, Intent arg1) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        isConnected.set(activeNetwork != null
                && activeNetwork.isConnectedOrConnecting());
        connectionType.set(isConnectedMobile(activeNetwork) ? ConnectionType.MOBILE.ordinal() :
                ConnectionType.WIFI.ordinal());
    }

    private boolean isConnectedWifi(NetworkInfo info) {
        return (info != null && info.isConnected() && info.getType() == ConnectivityManager.TYPE_WIFI);
    }

    private boolean isConnectedMobile(NetworkInfo info) {
        return (info != null && info.isConnected() && info.getType() == ConnectivityManager.TYPE_MOBILE);
    }

    public static ConnectionType getConnectionType() {
        return ConnectionType.values()[connectionType.get()];
    }

    public static boolean isConnected() {
        return isConnected.get();
    }
}
