package com.dnod.whoisthere.data

enum class Status {
    CONNECTED, DISCONNECTED, CONNECTING
}
