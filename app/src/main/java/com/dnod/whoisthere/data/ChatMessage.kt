package com.dnod.whoisthere.data

class ChatMessage {
    var body: String? = null
    var ownerId: String? = null
}
