package com.dnod.whoisthere;

import android.app.Application;
import com.dnod.whoisthere.utils.PreferencesHelper;

public class AppController extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        PreferencesHelper.init(this);
    }
}
