package com.dnod.whoisthere.ui

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.annotation.TargetApi
import android.support.v7.app.AppCompatActivity
import android.os.Build
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.text.TextUtils
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import com.dnod.whoisthere.R

import kotlinx.android.synthetic.main.activity_login.*
import android.os.IBinder
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import com.dnod.whoisthere.service.ChatService
import com.dnod.whoisthere.utils.PreferencesHelper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable

class LoginActivity : AppCompatActivity() {

    private val subscriptions = CompositeDisposable()
    private var chatService: ChatService? = null
    private val serviceConnection = object : ServiceConnection {

        override fun onServiceDisconnected(name: ComponentName) {
            chatService = null
        }

        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            val myBinder = service as ChatService.ChatBinder
            chatService = myBinder.service
            if (PreferencesHelper.getUserName() != null) {
                login(PreferencesHelper.getUserName(), PreferencesHelper.getUserPassword())
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        if (PreferencesHelper.getUserName() != null) {
            showProgress(true)
            bindService(Intent(this, ChatService::class.java), serviceConnection, Context.BIND_AUTO_CREATE);
            return
        }
        password.setOnEditorActionListener(TextView.OnEditorActionListener { _, id, _ ->
            if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                attemptLogin()
                return@OnEditorActionListener true
            }
            false
        })
        email_sign_in_button.setOnClickListener { attemptLogin() }
        bindService(Intent(this, ChatService::class.java), serviceConnection, Context.BIND_AUTO_CREATE);
    }

    override fun onDestroy() {
        super.onDestroy()
        if (chatService != null) {
            unbindService(serviceConnection)
        }
        subscriptions.clear()
    }

    private fun attemptLogin() {

        // Reset errors.
        username.error = null
        password.error = null

        // Store values at the time of the login attempt.
        val usernameStr = username.text.toString()
        val passwordStr = password.text.toString()

        var cancel = false
        var focusView: View? = null

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(passwordStr) && !isPasswordValid(passwordStr)) {
            password.error = getString(R.string.error_invalid_password)
            focusView = password
            cancel = true
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(usernameStr)) {
            username.error = getString(R.string.error_field_required)
            focusView = username
            cancel = true
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView?.requestFocus()
        } else {
            if (chatService == null) {
                Snackbar.make(root, "Failed to connect", Snackbar.LENGTH_LONG).show()
                return
            }
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true)
            login(usernameStr, passwordStr)
        }
    }

    private fun login(username: String, password: String) {
        subscriptions.add(chatService!!.login(username, password)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ status ->
                    if (status) {
                        val intent = Intent(this, ChatActivity::class.java)
                        startActivity(intent)
                        finish()
                    } else {
                        Snackbar.make(root, "Failed to login", Snackbar.LENGTH_LONG).show()
                    }
                }, { throwable ->
                    Snackbar.make(root, throwable.localizedMessage, Snackbar.LENGTH_LONG).show()
                }))
    }

    private fun isPasswordValid(password: String): Boolean {
        return password.length > 4
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private fun showProgress(show: Boolean) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            val shortAnimTime = resources.getInteger(android.R.integer.config_shortAnimTime).toLong()

            login_form.visibility = if (show) View.GONE else View.VISIBLE
            login_form.animate()
                    .setDuration(shortAnimTime)
                    .alpha((if (show) 0 else 1).toFloat())
                    .setListener(object : AnimatorListenerAdapter() {
                        override fun onAnimationEnd(animation: Animator) {
                            login_form.visibility = if (show) View.GONE else View.VISIBLE
                        }
                    })

            login_progress.visibility = if (show) View.VISIBLE else View.GONE
            login_progress.animate()
                    .setDuration(shortAnimTime)
                    .alpha((if (show) 1 else 0).toFloat())
                    .setListener(object : AnimatorListenerAdapter() {
                        override fun onAnimationEnd(animation: Animator) {
                            login_progress.visibility = if (show) View.VISIBLE else View.GONE
                        }
                    })
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            login_progress.visibility = if (show) View.VISIBLE else View.GONE
            login_form.visibility = if (show) View.GONE else View.VISIBLE
        }
    }
}
