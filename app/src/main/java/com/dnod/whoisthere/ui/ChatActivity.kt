package com.dnod.whoisthere.ui

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.IBinder
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.dnod.whoisthere.R
import com.dnod.whoisthere.data.Status
import com.dnod.whoisthere.data.User
import com.dnod.whoisthere.service.ChatService
import com.dnod.whoisthere.utils.PreferencesHelper
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.activity_chat.*
import io.reactivex.disposables.CompositeDisposable

class ChatActivity : AppCompatActivity() {

    private val subscriptions = CompositeDisposable()
    private var mChatAdapter: ChatAdapter? = null
    private var chatService: ChatService? = null
    private val serviceConnection = object : ServiceConnection {

        override fun onServiceDisconnected(name: ComponentName) {
            chatService = null
            updateStatus(Status.DISCONNECTED)
        }

        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            val myBinder = service as ChatService.ChatBinder
            chatService = myBinder.service
            btn_connect.visibility = View.VISIBLE
            subscibeToChat()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)
        setSupportActionBar(toolbar)
        updateStatus(Status.CONNECTED)
        input_message.addTextChangedListener(object : AbstractTextWatcher() {
            override fun afterTextChanged(s: Editable) { updateSendButtonState() }

        })
        btn_connect.setOnClickListener({
            if (chatService != null) {
                chatService!!.connect(companion.text.toString())
                supportActionBar!!.title = getString(R.string.title_chat, companion.text.toString())
                btn_connect.visibility = View.GONE
                companion.isEnabled = false
            }
        })
        btn_send.setOnClickListener({
            if (chatService != null) {
                chatService!!.send(input_message.text.toString())
                input_message.setText("")
            }
        })
        val layoutManager = LinearLayoutManager(this)
        layoutManager.setReverseLayout(true);
        list.setLayoutManager(layoutManager)
        mChatAdapter = ChatAdapter(this, PreferencesHelper.getUserName())
        list.adapter = mChatAdapter
        bindService(Intent(this, ChatService::class.java), serviceConnection, Context.BIND_AUTO_CREATE);
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.chat_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_logout -> {
                if (chatService != null) {
                    chatService!!.logout()
                }
                finish()
                return true
            }
        }
        return false
    }

    override fun onResume() {
        super.onResume()
        subscibeToChat()
    }

    override fun onPause() {
        super.onPause()
        subscriptions.clear()
        mChatAdapter!!.clear()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (chatService != null) {
            unbindService(serviceConnection)
        }
    }

    private fun subscibeToChat() {
        if (chatService != null) {
            subscriptions.add(chatService!!.subscribeConnectionState()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ status ->
                        updateStatus(status)
                    }, { throwable ->
                        Snackbar.make(root, throwable.localizedMessage, Snackbar.LENGTH_LONG).show()
                    }))
            subscriptions.add(chatService!!.subscribeChat()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ message ->
                        mChatAdapter!!.add(message)
                        list.smoothScrollToPosition(0)
                    }, { throwable ->
                        Snackbar.make(root, throwable.localizedMessage, Snackbar.LENGTH_LONG).show()
                    }))
        }
    }

    private fun updateSendButtonState() {
        val message = input_message.text.toString().replace("\\s+", "")
        btn_send.isEnabled = message.length > 0
    }

    private fun updateStatus(status: Status) {
        when (status) {
            Status.CONNECTED -> {
                this.status.setTextColor(ContextCompat.getColor(this, R.color.color_chat_connected))
                this.status.setText(R.string.lbl_status_connected)
            }
            Status.DISCONNECTED -> {
                this.status.setTextColor(ContextCompat.getColor(this, R.color.color_chat_disconnected))
                this.status.setText(R.string.lbl_status_disconnected)
            }
            Status.CONNECTING -> {
                this.status.setTextColor(ContextCompat.getColor(this, android.R.color.black))
                this.status.setText(R.string.lbl_status_connecting)
            }
        }
    }
}
