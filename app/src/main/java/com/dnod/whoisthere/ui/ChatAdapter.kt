package com.dnod.whoisthere.ui

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.dnod.whoisthere.R
import com.dnod.whoisthere.data.ChatMessage
import kotlinx.android.synthetic.main.item_outgoing_message.view.*
import kotlinx.android.synthetic.main.item_incoming_message.view.*

import java.util.ArrayList

class ChatAdapter(context: Context, private val mOwnerId: String) : RecyclerView.Adapter<ChatAdapter.Holder>() {

    private val layoutInflater: LayoutInflater
    private val mData = ArrayList<ChatMessage>()

    init {
        this.layoutInflater = LayoutInflater.from(context)
    }

    fun add(message: ChatMessage) {
        mData.add(0, message)
        notifyItemInserted(0)
    }

    fun clear() {
        mData.clear()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatAdapter.Holder {
        when (viewType) {
            0 -> return OutgoingHolder(layoutInflater.inflate(R.layout.item_outgoing_message, parent, false))
            1 -> return IncomingHolder(layoutInflater.inflate(R.layout.item_incoming_message, parent, false))
        }
        throw UnsupportedOperationException()
    }

    override fun onBindViewHolder(holder: ChatAdapter.Holder, position: Int) {
        holder.bind(mData.get(position))
    }

    override fun getItemCount(): Int {
        return mData.size
    }

    override fun getItemViewType(position: Int): Int {
        if (mData.get(position).ownerId.equals(mOwnerId))
            return 0;
        return 1
    }

    open abstract class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        abstract fun bind(message: ChatMessage)
    }

    class OutgoingHolder(itemView: View) : Holder(itemView) {
        val view: View = itemView

        override fun bind(message: ChatMessage) {
            view.outgoing_name.text = message.ownerId
            view.outgoing_body.text = message.body
        }
    }

    class IncomingHolder(itemView: View) : Holder(itemView) {
        val view: View = itemView

        override fun bind(message: ChatMessage) {
            view.incoming_name.text = message.ownerId
            view.incoming_body.text = message.body
        }
    }
}